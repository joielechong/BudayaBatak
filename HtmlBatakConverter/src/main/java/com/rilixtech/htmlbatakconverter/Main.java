package com.rilixtech.htmlbatakconverter;

import java.io.File;
import java.io.IOException;
import org.ini4j.Wini;

public class Main {

  public static void main(String[] args) {
    System.out.println("Start conversion...");

    // Read configuration
    Configuration config = readConfiguration();
    HtmlBatakConverter.convertHtmlToFiles(new File(config.getDirectoryName()), new File(config.getSaveDirectory()));
  }

  private static Configuration readConfiguration() {
    Wini ini = null;
    try {
      ini = new Wini(new File("config.ini"));
        String dir = ini.get("config", "homeDir");
        String saveDir = ini.get("config", "saveDir");
        Configuration configuration = new Configuration();
        configuration.setDirectoryName(dir);
        configuration.setSaveDirectory(saveDir);
        return configuration;
    } catch (IOException e) {
      e.printStackTrace();
    }

    return null;
  }
}
