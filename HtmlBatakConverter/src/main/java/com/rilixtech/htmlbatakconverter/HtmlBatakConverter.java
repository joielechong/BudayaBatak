package com.rilixtech.htmlbatakconverter;

import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * HTML Converter of Batak Dictionary from http://web.tiscali.it/batak/
 */
public class HtmlBatakConverter {

  private static final String UTF_8 = "UTF-8";

  public static void convertHtmlToFiles(File folder, File saveDirectory) {
    if (!saveDirectory.exists()) {
      saveDirectory.mkdir();
    }
    for (File file : folder.listFiles()) {
      List<Translation> translations = convertToList(file);
      writeToFile(saveDirectory + "/ " + getBaseName(file.getName()) + ".csv", translations);
    }
  }

  private static void writeToFile(String fileName, List<Translation> translations) {
    BufferedWriter bufferedWriter = null;
    try {
      bufferedWriter = new BufferedWriter(new FileWriter(fileName));
      Writer writer = new FileWriter(fileName);

      ColumnPositionMappingStrategy<Translation> mappingStrategy =
          new ColumnPositionMappingStrategy<>();
      mappingStrategy.setType(Translation.class);
      String[] columns = new String[] { "word", "meaning" };
      mappingStrategy.setColumnMapping(columns);

      StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder<Translation>(writer)
          .withMappingStrategy(mappingStrategy)
          .build();

      beanToCsv.write(translations);

      writer.close();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (CsvRequiredFieldEmptyException e) {
      e.printStackTrace();
    } catch (CsvDataTypeMismatchException e) {
      e.printStackTrace();
    } finally {
      try {
        bufferedWriter.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private static List<Translation> convertToList(File file) {
    System.out.println("Converting file " + file.getName());
    try {
      List<Translation> translations = new ArrayList<>();
      Document doc = Jsoup.parse(file, UTF_8);

      Element body = doc.body();
      Elements paragraphs = body.getElementsByTag("P");

      for (Element element : paragraphs) {
        Elements tagElements = element.getElementsByTag("i");
        String word = "";
        if (tagElements != null) {
          if (tagElements.first() != null) {
            word = tagElements.first().text();
            if (word == null) continue;
          } else {
            continue;
          }
        }
        String meaning = element.ownText();

        meaning = meaning.replace(word, "");

        Translation translation = new Translation(word, meaning);
        translations.add(translation);
      }
      return translations;
    } catch (IOException e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Gets the base name, without extension, of given file name.
   * <p/>
   * e.g. getBaseName("file.txt") will return "file"
   *
   * @return the base name
   */
  public static String getBaseName(String fileName) {
    int index = fileName.lastIndexOf('.');
    if (index == -1) {
      return fileName;
    } else {
      return fileName.substring(0, index);
    }
  }
}
