package com.rilixtech.htmlbatakconverter;

public class Translation {
  private final String word;
  private final String meaning;

  public Translation(String word, String meaning) {
    this.word = word;
    this.meaning = meaning;
  }

  public String getWord() {
    return word;
  }

  public String getMeaning() {
    return meaning;
  }
}
