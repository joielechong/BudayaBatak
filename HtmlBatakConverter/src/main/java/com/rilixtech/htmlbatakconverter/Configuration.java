package com.rilixtech.htmlbatakconverter;

public class Configuration {
 private String directoryName;
 private String saveDirectory;

  public Configuration() {
    this.directoryName = "";
    saveDirectory = "";
  }

  public String getDirectoryName() {
    return directoryName;
  }

  public void setDirectoryName(String directoryName) {
    this.directoryName = directoryName;
  }

  public String getSaveDirectory() {
    return saveDirectory;
  }

  public void setSaveDirectory(String saveDirectory) {
    this.saveDirectory = saveDirectory;
  }
}
